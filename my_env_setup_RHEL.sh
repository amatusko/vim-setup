#!/bin/bash
set -e
set -x

touch $HOME/.profile
touch $HOME/.bash_profile
touch $HOME/.bashrc

# Determine Linux Flavz
export OS=`uname -a`
### Determine System Hardware Architecture ###
export OSARCH=`uname -m`

# If OSX install and use brew for command line setup
if [ {$OS} == "Darwin" ]; then
  echo "We have a Mac!"
  
  ### Install Homebrew and Homebrew Cask. ###
  echo "Install Homebrew and Homebrew Cask."
  /usr/local/bin/ruby -e '$(curl -fsSL "https://raw.githubusercontent.com/Homebrew/install/master/install)'
  PATH=/usr/local/bin:$PATH
  export "PATH=/usr/local/bin:$PATH" >> $HOME/.profile
  export INSTALLER="brew"
  
  ### Update Homebrew / Brew ### 
  brew update
  brew upgrade

  ### update brew sources and "casks" ### 
  brew tap caskroom/cask
  brew tap homebrew/core

  ### Install shit w/ brew ###
  brew install caskroom/cask/brew-cask bash bash-completion cask 
  brew install git ruby curl wget vim@7.4 neovim/neovim/neovim git-flow dos2unix calc 
  brew install git-credential-manager git-extras git-utils zsh-git-prompt github-keygen bash-git-prompt git-crypt
  brew install zsh zsh-autosuggestions zsh-completions zsh-git-prompt zsh-history-substring-search speedtest_cli aws-shell 
  brew install zsh-lovers zsh-navigation-tools zsh-syntax-highlighting 
  ### Node Stuff ###
  brew install node@6 node-build nodebrew nodeenv nodenv
  brew install nmap nodejs multimarkdown 
  ### AWS Tools ###
  brew install awscli aws-as aws-cfn-tools aws-cloudsearch aws-elasticache aws-keychain aws-mon aws-shell 
  brew install aws-sns-cli awslogs yaws kube-aws
  brew install hh lftp geoip htop pv screen tmux tmuxinator-completion tmux-mem-cpu-load tree tree unrar zip rsync vdirsyncer 
  brew install ack colordiff coreutils moreutils findutils pdf-redact-tools pdftohtml pdfgrep mupdf mupdf-tools 
  brew install diff-pdf pdfcrack imagemagick
  brew install gpg jq automake autoconf jpegoptim gnu-sed --with-default-names 
  brew install mackup z hub homebrew/dupes/grep homebrew/dupes/openssh homebrew/dupes/screen dark-mode git-lfs
  brew install p7zip rename ssh-copy-id ruby-build groovy groovysdk groovyserv 
  brew install cowsay the_silver_searcher watch siege postgres mysql apache  
  brew install terraform terraform-docs terraform-inventory 
  brew install stow rsnapshot  

  brew tap caskroom/fonts
  brew cask install flux caffeine cheatsheet blockblock knockknock appcleaner font-hack font-source-code-pro
  brew cask install alfred cert-quicklook vlc-nightly betterzipql qlcolorcode qlimagesize qlmarkdown qlstephen 
  brew cask install quicklook-csv bettertouchtool

  ### Fuck it! Let's install php70! ###
  echo "Fuckit. Why not install php70?"
    brew install homebrew/php/php70-grpc homebrew/php/php70-hprose homebrew/php/php70-http homebrew/php/php70-igbinary
    brew install homebrew/php/php70-imagick homebrew/php/php70-intl homebrew/php/php70-ioncubeloader homebrew/php/php70-kafka
    brew install homebrew/php/php70-libsodium homebrew/php/php70-lua homebrew/php/php70-lz4 homebrew/php/php70-lzf
    brew install homebrew/php/php70-mailparse homebrew/php/php70-maxminddb homebrew/php/php70-mcrypt homebrew/php/php70-mecab
    brew install homebrew/php/php70-memcache homebrew/php/php70-memcached homebrew/php/php70-mongodb homebrew/php/php70-msgpack
    brew install homebrew/php/php70-mustache homebrew/php/php70-oauth homebrew/php/php70-opcache homebrew/php/php70-pcntl
    brew install homebrew/php/php70-pdo-dblib homebrew/php/php70-pdo-pgsql homebrew/php/php70-phalcon
    brew install homebrew/php/php70-propro homebrew/php/php70-rdkafka
    brew install homebrew/php/php70-pspell homebrew/php/php70-pthreads homebrew/php/php70-raphf 
    brew install homebrew/php/php70-redis homebrew/php/php70-ref homebrew/php/php70-scrypt homebrew/php/php70-snmp
    brew install homebrew/php/php70-solr homebrew/php/php70-ssh2 homebrew/php/php70-stats homebrew/php/php70-swoole
    brew install homebrew/php/php70-tidy homebrew/php/php70-timecop homebrew/php/php70-timezonedb homebrew/php/php70-trace
    brew install homebrew/php/php70-uopz homebrew/php/php70-uuid homebrew/php/php70-uv homebrew/php/php70-v8js
    brew install homebrew/php/php70-xdebug homebrew/php/php70-xmldiff
    brew install homebrew/php/php70-xxtea homebrew/php/php70-yac
    brew install homebrew/php/php70-yaf homebrew/php/php70-yaml

    brew cask install iterm2 google-chrome chrome-devtools chrome-remote-desktop-host dmm-player-for-chrome macvim virtualbox dockutil
    brew cask install dropbox evernote firefox google-drive kindle omnigraffle skype slack spectacle spotify sublime-text3 the-unarchiver

    brew cask install java intellij-idea docker-edge jetbrains-toolbox owncloud font-inconsolata-dz-for-powerline youtube-dl mc

    brew install docker-toolbox docker docker-clean docker-cloud docker-completion docker-machine docker-machine-completion 
    brew install docker-machine-driver-xhyve docker-machine-nfs docker-machine-parallels docker-swarm boot2docker-completion 
    brew install docker-compose docker-compose-completion 
    dockutil --remove all # to cleanup the dock bar
  
fi


### If CentOS, RHEL, or Amazon Linux, create a function to use yum for setup ###


### Common core stuff ###
export GIT_SSL_NOVERIFY=true

export PIP=$(which pip3)
export PATH=$PIP:$PATH
$PIP install --upgrade pip
### If that doesn't faildozer, then use pip to install ansible ###
#$PIP install awscli ansiblator ansible-troll 
$PIP install ansible ansible-roles-graph pytest-ansible-playbook ansible-review
if [ {$OS} == "Darwin" ]; then
  brew cask install ansible-dk
  brew install ansible-cmdb terraform-provisioner-ansible
fi
# playbook_assistant
# ansidoc test-ansible ansible-conductor ansibleapi ansiblereporter nexus_ansible
$PIP install ansible-flow ansible-marathon ansible-modules-hashivault
$PIP install ansible-windows-compat ansible-inventory pocker-ansible foreman-ansible-inventory ansible-bundle
$PIP install ansible-library jenkins-job-builder-ansible ansible-subprocess ansible-shell ansible-lint ansible_inventory_creator
$PIP install ansible-docgen ansible-runner ansible-role-manager ansible-container ansible-vault ansible_importer
$PIP install ansible-cmdb ansible-role ansible-playbook-debugger ansible-tools ansible-inventory-grapher ansible_role_apply
$PIP install ansible-galaxy-local-deps ansible-toolset ansible-vagrant ansible-ec2-inventory ansible_role_installer ansideps
$PIP install ansible-toolkit ansible-modules-dcos ansible-dynamic-inventory ansible-universe 
$PIP install ansigenome ansiscaf ansitom arpm bosh_inventory bossimage cyclosible datemike dauber ec2ansible ecsutils
$PIP install fabric-tb futen galaxy-updater infrared linchpin LintPlaybook messier muon ndmtk nestedfacts 
$PIP install osxstrap packsible ploy_ansible pyansible Pyventory roler supervisorclusterctl 

# Setting up my precious .vim / neovim  configuration 
mv $HOME/.vim __vim
git clone --recursive https://amatusko@bitbucket.org/amatusko/vim-setup.git $HOME/.vim/
ln -s $HOME/.vim/.vimrc $HOME/.vimrc
rm -rf $HOME/__vim
 
### Setting up oh-my-zsh: ###
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

#sudo su - 
#mv $HOME/.vim $HOME/__vim
#ln -s $HOME/.vim/.vimrc $HOME/.vimrc


### Install Antigen for ZSH Plugin Management ###
mkdir $HOME/.zsh

curl -L git.io/antigen > $HOME/.zsh/antigen.zsh
ln -s $HOME/.zsh/antigen.zsh antigen.zsh

export PATH=$HOME/.zsh:$PATH
chmod +x antigen.zsh
source antigen.zsh
touch $HOME/.antigenrc
antigen cache-gen
antigen bundle git
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle robbyrussell/oh-my-zsh plugins/ruby
antigen bundle kennethreitz/autoenv
# or `antigen bundle Tarrasch/zsh-autoenv`

### Load zsh themes with antigen ###
antigen theme XsErG/zsh-themes themes/lazyuser

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle heroku
antigen bundle pip
antigen bundle lein
antigen bundle command-not-found
### Think this is an alternative to oh-my-zsh??? ###
### Can be found here: https://github.com/sorin-ionescu/prezto ###
antigen use prezto
antigen bundle sorin-ionescu/prezto modules/helper  # required for Git module
antigen bundle sorin-ionescu/prezto modules/editor
antigen bundle sorin-ionescu/prezto modules/git
antigen bundle sorin-ionescu/prezto modules/prompt
antigen apply

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting

# Tell Antigen that you're done.
antigen apply
# git clone --recursive https://github.com/zsh-users/zsh-syntax-highlighting.git $HOME/.zsh/
