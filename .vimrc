set shell=/bin/bash
" Vundle
set nocompatible
filetype off
" set the runtime path to include Vundle and initialize
"set rtp+=~/.vim/bundle/Vundle.vim
" call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
"PLUG
"let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
""  if empty(glob(data_dir . '/autoload/plug.vim'))
""  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
""  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
"endif
" Install vim-plug if not found
"if empty(glob('~/.vim/autoload/plug.vim'))
""  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
""    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"endif

" Run PlugInstall if there are missing plugins
"autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
""  \| PlugInstall --sync | source $MYVIMRC
""\| endif

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

Plug 'hashivim/vim-terraform.vim'
Plug 'godlygeek/tabular'
Plug 'Chiel92/vim-autoformat'
Plug 'altercation/vim-colors-solarized'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'rafi/awesome-vim-colorschemes'
Plug 'pearofducks/ansible-vim'
Plug 'm-kat/aws-vim'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-surround'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'raimondi/delimitmate'
Plug 'flazz/vim-colorschemes'
Plug 'christoomey/vim-tmux-navigator'
Plug 'elzr/vim-json'
Plug 'leafgarland/typescript-vim'
Plug 'hail2u/vim-css3-syntax'
Plug 'jelera/vim-javascript-syntax'
Plug 'klen/python-mode'
Plug 'noahfrederick/vim-composer'
Plug 'hiphish/jinja.vim'
Plug 'othree/javascript-libraries-syntax.vim'
Plug 'dsawardekar/wordpress.vim'
Plug 'tomasr/molokai'
Plug 'joshdick/onedark.vim'
Plug 'hashivim/vim-hashicorp-tools'

  "The following are examples of different formats supported.
  "Keep Plugin commands between vundle#begin/end.
  "plugin on GitHub repo
Plug 'tpope/vim-fugitive'
  "plugin from http://vim-scripts.org/vim/scripts.html
  "Git plugin not hosted on GitHub
Plug 'git://git.wincent.com/command-t.git'
  "git repos on your local machine (i.e. when working on your own plugin)
  "Plugin 'file:///home/gmarik/path/to/plugin'
  "The sparkup vim script is in a subdirectory of this repo called vim.
  "Pass the path to set the runtimepath properly.
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
  "different version somewhere else.
filetype plugin indent on

"Terraform stuff
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_remap_spacebar=1
autocmd FileType terraform setlocal commentstring=#%s

" Following lines added by drush vimrc-install on Thu, 16 Oct 2014 18:39:34 +0000.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Toggle NERDTree
map <C-n> :NERDTreeToggle<CR>
" How can I close vim if the only window left open is a NERDTree?
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
" Gundo toggle undo tree
nnoremap <F5> :GundoToggle<CR>
" airline
let g:airline#extensions#tabline#enabled = 1
let g:AWSVimValidate = 1
" 
" Custom Plugins
"
" Utilsnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"
" Vim Supertab
let g:SuperTabDefaultCompletionType = "<c-n>"
" Groovy
au BufNewFile,BufRead *.groovy  setf groovy 
" Custom Themes

"Plugin 'carakan/new-railscasts-theme'

" JS Linter
let g:javascript_plugin_jsdoc = 1
"Node.js for Vim settings
let g:nodejs_complete_config = { 'js_compl_fn': 'jscomplete#CompleteJS', 'max_node_compl_len': 15 }

syn on
set ruler
set tabstop=2
set expandtab
set shiftwidth=2
" set ai
set hlsearch
set incsearch
set textwidth=0
set wrapmargin=0
set autowrite
set highlight=l:ErrorMsg
set ignorecase
set smartcase
set scrolloff=2
set wildmode=longest,list
set nocompatible
set noswapfile
set backspace=indent,eol,start

" display settings
set background=dark
" enable for dark terminals
set nowrap              " dont wrap lines 
set scrolloff=2         " 2 lines above/below cursor whe
set number              " show line number
set showmatch
" show matching bracket (briefly jump)
set showmode            " show mode in status bar (insert/replace/..
set showcmd             " show typed command in status b
set ruler               " show cursor position in status ba
set title               " show file in titleba
set wildmenu            " completion with me
set wildignore=*.o,*.obj,*.bak,*.exe,*.py[co],*.swp,
set laststatus=2        " use 2 lines for the status bar set matchtime=2      
" # show matching bracket for 0.2 second
set matchpairs+=<:>
set confirm

"Enable iTerm to use 256 colors in vim
set t_Co=256

" I prefer koheler, murphy but setting to slate so I don't mistake dev for prod with both windows open
" let ayucolor=dark   " for dark version of theme 
"colorscheme eldar
colorscheme colorful256

filetype plugin on

" From http://statico.github.io/vim.html
" Up and down keys go to wrapped section of line insted of skipping lines... Big annoyance!
:nmap j gj
:nmap k gk
" Use \l to toggle line numbers
:nmap \l :setlocal number!<CR>
" Use \o to toggle paste mode
:nmap \o :set paste!<CR>
" a key which toggles the visibility of the tree:
:nmap \e :NERDTreeToggle<CR>
" Everybody has a color scheme they’re comfortable with. Modern terminals support 256 colors, but sometimes you need to kick Vim to recognize this:
if $TERM == "xterm-256color" || $TERM == "screen-256color" || $COLORTERM == "gnome-terminal"
  set t_Co=256
endif

" fugitive git bindings
nnoremap <space>ga :Git add %:p<CR><CR>
nnoremap <space>gs :Gstatus<CR>
nnoremap <space>gc :Gcommit -v -q<CR>
nnoremap <space>gt :Gcommit -v -q %:p<CR>
nnoremap <space>gd :Gdiff<CR>
nnoremap <space>ge :Gedit<CR>
nnoremap <space>gr :Gread<CR>
nnoremap <space>gw :Gwrite<CR><CR>
nnoremap <space>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <space>gp :Ggrep<Space>
nnoremap <space>gm :Gmove<Space>
nnoremap <space>gb :Git branch<Space>
nnoremap <space>go :Git checkout<Space>
nnoremap <space>gps :Dispatch! git push<CR>
nnoremap <space>gpl :Dispatch! git pull<CR>

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
let g:indentLine_char = '⦙' "Indentation guides, may be annoying"
  
