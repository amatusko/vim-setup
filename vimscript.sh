#!/bin/bash 

PLATFORM="$(uname -s)"

echo "Install Homebrew"
# Should check if homebrew is installed first and if not, install it. 
command -v brew >/dev/null 2>&1 || { echo >&2 "Installing Homebrew Now"; \
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"; } || true
# ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

platform='unknown' 
unamestr=`uname -s`

if [[ "$unamestr" == 'Linux' ]]; then
   platform='linux'
   alias brew="apt-get "
   apt-get install ruby git awscli vim zsh dos2unix htop traceroute tmux tree unzip 
elif [[ "$unamestr" == 'Darwin' ]]; then
   platform='Darwin'
fi

echo "Install xcode"
xcode-select --install || true
echo "Updates the package list from the repository"
brew update --merge || true
brew upgrade --cleanup || true



#bi is short for brew install
bi()
{
        PROGRAM=$1
        if brew ls --versions $PROGRAM  > /dev/null; then
          # The package is installed
          echo "Curl is installed. Moving on to the next one..."
        else
          echo "Curl is not installed. Installing now..."
          brew install --verbose curl wget || true
        fi
        exit 1
}

if [[ "$unamestr" == 'Darwin' ]]; then
	echo "Install xcode"
	xcode-select --install || true
	echo "Updates the package list from the repository"
	brew update --merge || true
	brew upgrade --cleanup || true

	bi curl
	bi zsh
	bi wget
	bi bash-completion
	#brew install --verbose \  #zsh git vim wget bash-completion            
	bi awscli
	bi ccryptdos2
	bi unix
	bi geoip
	bi git-flow
	bi hh
	bi htop    
	bi jq
	bi lftp
	bi ncdu
	bi nmap
	bi tig
	bi tmux
	bi tree
	bi unrar
	bi unzip
	bi xz
	bi vimpager
	bi ack
	bi colordiff
	bi coreutils
	bi gawk        
	bi cowsay
	bi axel
	bi rlwrap
	bi mackup
	bi macvim
	bi tmux
	bi speedtest-cli
	bi hub
	bi automake
	bi autoconf
	bi watch
	bi python
	bi python3   
	bi xquartz
	bi dockutil
	bi gpg
	bi jq
	bi ctop
	bi dnstop
	bi gnu-getopt
	bi innotop
	bi jvmtop
	bi sntop
	bi sqtop
	bi pg_top
	bi libgtop
	bi iftop   
	bi darkmode
	bi coreutils
	bi moreutils
	bi findutils
	bi gnu-sed --with-default-names
	bi vim --override-system-vi
	bi macvimhomebrew/dupes/
	bi grephomebrew/dupes/
	bi opensshhomebrew/dupes/
	bi screen
	bi dark-mode
	bi git-lfs
	bi p7
	bi zip
	bi rename
	bi ssh-copy-id
	bi tree
	bi rbenv
	bi ruby-build
	bi rbenv-default-gems
	bi watch
	bi archey
	bi thefuck
	bi trash
	bi fortune
	bi pyenv
	bi pyenv-virtualenv
	bi rsnapshot
	bi stow
	bi github-keygen
	bi dropbox-uploader
	bi chrome-cli
	bi google-sql-tool
	bi docker-machine-parallelsboot2
	bi dockerboot2
	bi docker-completion
	bi docker-compose
	bi docker-compose-completion
	bi docker-machine
	bi docker-machine-completion
	bi docker-machine-nfs
	bi docker-squash
	bi docker-swarm
	bi docker-ls
	bi docker-cloud
	bi docker-credential-helper
	bi app-engine-java
	bi google-java-format
	bi jslint4
	bi java
	bi ansible
	bi ansible-lint
	bi ansible-cmdb
	bi aws-cfn-tools
	bi aws-apigateway-importer
	bi aws-as
	bi aws-as
	bi aws-cloudsearch
	bi aws-elasticache
	bi aws-elasticbeanstalk
	bi aws-es-proxy
	bi aws-keychain
	bi aws-mon
	bi aws-sdk-cpp
	bi aws-shell
	bi aws-sns-cli
	bi awscli
	bi awslogs
	bi kube-aws
	bi yaws
	bi desktop-file-util
	bi smartmontools
	bi go
	bi terraform
	bi terraform-docs
	bi terraform-inventory
	bi terraform-provisioner-ansible
	bi terraform_landscape
	bi terraforming
	bi osxutils
	bi dockutil
	bi yamllint
	bi jsonlintpdf2
	bi json
	bi rapidjson
	bi git-sh
	bi git-subrepohtml2text
        
        brew cask install caskroom/cask/sublime-text
        brew cask install caskroom/cask/big-mean-folder-machine
        brew cask install caskroom/cask/caffeine
        brew cask install caskroom/cask/dropbox
        brew cask install caskroom/cask/evernote
        brew cask install caskroom/cask/firefox
        brew cask install caskroom/cask/google-chrome
        brew cask install caskroom/cask/chrome-devtools
        brew cask install caskroom/cask/dmm-player-for-chrome
        brew cask install caskroom/cask/chrome-remote-desktop-host
        brew cask install caskroom/cask/google-chat
        brew cask install caskroom/cask/google-hangouts
        brew cask install caskroom/cask/google-cloud-sdk
        brew cask install caskroom/cask/google-photos-backup-and-sync
        brew cask install caskroom/cask/google-web-designer
        brew cask install caskroom/cask/google-nik-collectionu
        brew cask install caskroom/cask/google-drive-file-stream
        brew cask install caskroom/cask/iterm2
        brew cask install caskroom/cask/kindle
        brew cask install caskroom/cask/omnigraffle
        brew cask install caskroom/cask/skype
        brew cask install caskroom/cask/slack
        brew cask install spectacle
        brew cask install caskroom/cask/spotify
        brew cask install caskroom/cask/sublime
        brew cask install caskroom/cask/sublime-text
        brew cask install caskroom/cask/the-unarchiver
        brew cask install caskroom/cask/parallels
        brew cask install caskroom/cask/parallels-client
        brew cask install caskroom/cask/parallels-access
        brew cask install caskroom/cask/virtualbox
        brew cask install caskroom/cask/virtualbox-extension-pack
        brew cask install caskroom/cask/intellij-idea
        brew cask install caskroom/cask/pycharm
        brew cask install caskroom/cask/webstorm
        brew cask install caskroom/cask/adobe-creative-cloud
        brew cask install caskroom/cask/microsoft-office
        brew cask install caskroom/cask/docker
        brew cask install caskroom/cask/docker-toolbox
        brew cask install caskroom/cask/java
        brew cask install caskroom/cask/java-jdk-javadoc
        brew cask install caskroom/cask/ansible-dk
        brew cask install caskroom/cask/macdown
        brew cask install caskroom/cask/sourcetree
        brew cask install caskroom/cask/atom
        brew cask install caskroom/cask/tableplus
        brew cask install caskroom/cask/github
        brew cask install caskroom/cask/bbedit
        brew cask install caskroom/cask/alfred
        brew cask alfred link
        brew update && brew upgrade
fi

        #Install Oh My Zsh
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
        echo "source fortune" >> .bash_profile
        echo "source fortune" >> .zshrc
#git clone https://github.com/octplane/ansible_stdout_compact_logger.git /usr/local/bin/ansible_stdout_compact_logger

#echo "SHOW all of the possible homebrew casks to install."
#for cask in $(brew cask search); do
#    brew cask info $cask;
#done

### Node Love ###
ls -1 /usr/local/node_modules > ~/node_modules.txt
rm -rf /usr/local/lib/node_modules
brew uninstall node
brew install node --without-npm
echo prefix=~/.npm-packages >> ~/.npmrc
curl -L https://www.npmjs.com/install.sh | sh
for i in cat $HOME/node_modules.txt; do npm install -g $i;done
export PATH="$HOME/.npm-packages/bin:$PATH"

wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.xx.0/install.sh | bash

cd $HOME


VIMDIR=".vim"

echo "Delete old vim_old dir if exists"
OLDVIMDIR="vim_old"
if [[ -d "$OLDVIMDIR" ]] ; then
  rm -rf "$OLDVIMDIR"
fi

echo "Does the .vim directory already exist? If so copy it to vim_old"
if [[ -d "$VIMDIR" ]] ; then
  echo "Old vim dir found"
  mv .vim vim_old
fi

VIMRC=.vimrc
ZSHRC=.zshrc

if [ -h "$HOME/$VIMRC" ] ; then
  echo ".vimrc symlink exists, unlink it."
  unlink ~/.vimrc
fi
if [ -h "$HOME/$ZSHRC" ] ; then
  echo ".zshrc symlink exists, unlink it."
    unlink ~/.zshrc
    mv ~/.zshrc ~/_zshrc
fi

echo "Now we can download and install the pack."
git clone --recursive https://amatusko@bitbucket.org/amatusko/vim-setup.git $HOME/.vim/
 
echo "Download Vundle Manually \n"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

echo "Run the vundle install from command line"
vim +PluginInstall +qall

echo "Symlink the .zshrc and .vimrc files inside the pack"
cd ~
ln -s $HOME/.vim/.vimrc $HOME/.vimrc
ln -s $HOME/.vim/.zshrc $HOME/.zshrc

gem install bropages
gem install lunchy
brew search aws > awsfunctions.txt
brew install $(cat awsfunctions.txt);
brew install macvim --with-override-system-vim
brew install vim git curl wget the_silver_searcher mtr --no-gtk

brew cleanup --force

#[[ if -f $HOME/.bashrc ]]
  mv $HOME/.vimrc old_vimrc
  unlink $HOME/.vimrc
  ln -s $HOME/.vim/.bashrc $HOME/.bashrc
  source $HOME/.bashrc
#fi

#[[ if -f $HOME/.bash_profile ]]
  mv $HOME/.bash_profile old_bash_profile
  unlink $HOME/.bash_profile
  ln -s $HOME/.vim/.bash_profile $HOME/.bash_profile
  source $HOME/.bash_profile
#fi

#[[ if -f $HOME/.profile ]]
  mv $hOME/.profile old_profile
  unlink  $HOME/.profile
  ln -s $HOME/.vim/.bash_profile $HOME/.profile
  source $HOME/.profile
#else

update() {
    local brew="brew update; brew upgrade;"
    local gisty="gisty pull_all; gisty sync_delete"
    local gem="gem update;"
    local pip="pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs pip install -U -q"
    sh -c $brew$gisty; sudo sh -c $gem$pip
}

cdf() {
    target=`osascript -e 'tell application "Finder" to if (count of Finder windows) > 0 then get POSIX path of (target of front Finder window as text)'`
    if [ "$target" != "" ]; then
        cd "$target"; pwd
    else
        echo 'No Finder window found' >&2
    fi
}

update

